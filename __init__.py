# Import statements: the list of outside modules you'll be using in your
# skills, whether from other files in mycroft-core or from external libraries
from os.path import dirname
from os.path import join
from os.path import expanduser
from os import makedirs

from subprocess import call
from subprocess import Popen

from adapt.intent import IntentBuilder
from mycroft.skills.core import MycroftSkill
from mycroft.util.log import getLogger

__author__ = 'edaubert'

# Logger: used for debug lines, like "LOGGER.debug(xyz)". These
# statements will show up in the command line when running Mycroft.
LOGGER = getLogger(__name__)


# The logic of each skill is contained within its own class, which inherits
# base methods from the MycroftSkill class with the syntax you can see below:
# "class ____Skill(MycroftSkill)"
class SpotifySkill(MycroftSkill):

    # The constructor of the skill, which calls MycroftSkill's constructor
    def __init__(self):
        super(SpotifySkill, self).__init__(name="SpotifySkill")

    def set_spotify_cli_configuration(self):
        if self.settings['SP_ID'] != "" and self.settings['SP_ID'] is not None:
            home = expanduser("~")
            try:
                makedirs(join(home, ".config", "spotify-cli"))
            except OSError:
                pass

            configuration = open(
                join(home, ".config", "spotify-cli", "configuration"), "w")
            configuration.write("SP_ID='" + self.settings['SP_ID'] + "'")
            configuration.write("\n")
            configuration.write("SP_SECRET='" + self.settings['SP_SECRET'] +
                                "'")
            configuration.write("\n")
            configuration.close()

            if call([
                    self.script, "check_user_authentication", ">", "/dev/null"
            ]) is not 0:
                self.server = Popen(
                    [self.script, "start_server", ">", "/dev/null"])
                self.speak_dialog("app.authentication.require")
        else:
            self.speak_dialog("app.authentication.require")

    # This method loads the files needed for the skill's functioning, and
    # creates and registers each intent that the skill uses
    def initialize(self):
        # get the configuration from settings
        # and store them to be available by spotify-cli script
        self.script = join(dirname(__file__), "spotify-cli", "sp")

        self.set_spotify_cli_configuration()

        # recommended in doc but not needed...
        # self.load_data_files(dirname(__file__))

        pause_intent = IntentBuilder("PauseIntent")\
            .require('SpotifyKeyword')\
            .require("PauseKeyword")\
            .build()
        self.register_intent(pause_intent, self.handle_pause)

        play_intent = IntentBuilder("PlayIntent")\
            .require('SpotifyKeyword')\
            .require("PlayKeyword")\
            .build()
        self.register_intent(play_intent, self.handle_play)

        previous_intent = IntentBuilder("PreviousIntent")\
            .require('SpotifyKeyword')\
            .require("PreviousKeyword")\
            .build()
        self.register_intent(previous_intent, self.handle_previous)

        next_intent = IntentBuilder("NextIntent")\
            .require('SpotifyKeyword')\
            .require("NextKeyword")\
            .build()
        self.register_intent(next_intent, self.handle_next)

        search_intent = IntentBuilder("SearchIntent")\
            .require('SpotifyKeyword')\
            .require('SearchKeyword')\
            .require('Type')\
            .require('Query')\
            .build()
        self.register_intent(search_intent, self.handle_search)

    # The "handle_xxxx_intent" functions define Mycroft's behavior when
    # each of the skill's intents is triggered: in this case, he simply
    # speaks a response. Note that the "speak_dialog" method doesn't
    # actually speak the text it's passed--instead, that text is the filename
    # of a file in the dialog folder, and Mycroft speaks its contents when
    # the method is called.
    def handle_play(self, message):
        call([self.script, "play"])

    def handle_pause(self, message):
        call([self.script, "pause"])

    def handle_previous(self, message):
        call([self.script, "previous"])

    def handle_next(self, message):
        call([self.script, "next"])

    def handle_search(self, message):
        search_type = message.data.get("Type").strip().replace(" ", "-")
        search_query = message.data.get("Query")
        call([self.script, search_type, search_query])

    # The "stop" method defines what Mycroft does when told to stop during
    # the skill's execution.
    def stop(self):
        if self.server is not None:
            self.server.kill()


# The "create_skill()" method is used to create an instance of the skill.
# Note that it's outside the class itself.
def create_skill():
    return SpotifySkill()
