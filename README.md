# Spotify CLI

This skill offers interaction with spotify using [Spotify-CLI](https://gitlab.com/my-reactivity/spotify-cli).

## Description 

This skill offers basic interactions like play/pause/next/previous.

It also provides a way to ask spotify to play music from a specific type of source and from a query.

**The `spotify` word is mandatory on each interaction.**

## Examples 
* "`Play music on Spotify`"
* "`Pause the music on Spotify`"
* "`Stop the music on Spotify`"
* "`Go to the previous track on Spotify`"
* "`Go to the next track on Spotify`"
* "`On spotify search for artist Nightwish`"
* "`On spotify search for album Once Nightwish`"

## Known issues
 - Some intent are get by the Playback control instead of this skill so if you don't need it, please uninstall `playback-control` skill.

## TODO
 - spotify installation
 - current track
 - Search
  - by title
  - by recommendations
  ...
  
## How to install

Using msm is the easiest way:
```
msm install https://gitlab.com/my-reactivity/spotify-cli-skill.git
```

## How to configure it
Please follow the information on home.mycroft.ai/skills where you will find everything you need to configure the spotify-cli skill

### Using the hard way
You can configure spotify-cli by hand if you prefer.
Please have a look to the [configuration](https://gitlab.com/my-reactivity/spotify-cli#configuration)

## Credits 
Erwan Daubert, erwan.daubert@gmail.com, @edaubert

## Require
Currently this skill only works on a `desktop` installation of mycroft due to authentication issues and spotify installation issue.
